package org.ghc.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class RemoveDuplicatesUrl {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveDuplicatesUrl.class);
    private LinkedHashSet<String> listUrl = new LinkedHashSet<>();
    private LinkedList<String> listLines = new LinkedList<>();
    private static int startIndex = 0;
    private static int endIndex=0;

    public void removeDuplicateUrls() {

        File[] crawlDir = new File(".//crawl").listFiles();

        try {
            new File(".//crawl//crawl.txt").delete();
            LOGGER.info("Existing crawl.txt file deleted successfully");
        } catch (Exception exception) {
            LOGGER.info("Exception occurred while deleting crawl.txt files",exception);

        }

        if (crawlDir != null) {
            for (File file:crawlDir) {

                try (FileReader fileReader = new FileReader(file);
                     BufferedReader bufferedReader = new BufferedReader(fileReader)) {

                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        try {
                            String st[] = line.split(",");
                            if (listUrl.add(st[0])) {

                                listLines.add(line);
                                endIndex++;
                            }
                        } catch (Exception e) {
                            LOGGER.info("Exception occurred while removing duplicate urls=" + e.getMessage());

                        }
                    }

                } catch (Exception e) {
                    LOGGER.info("Exception occurred while reading crawl files=" + e.getMessage());
                }
                file.delete();
            }
        }

        try (FileOutputStream fileOutputStream = new FileOutputStream(".//crawl//crawl.txt")) {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

            for (int i = startIndex; i < endIndex; i++) {

                bw.write(listLines.get(i));
                bw.newLine();
                startIndex++;
            }
            bw.flush();
            bw.close();
        } catch (Exception e) {
            LOGGER.error("Exception occurred while generating single crawl text files=" + e.getMessage(), e);

        }

    }
}
