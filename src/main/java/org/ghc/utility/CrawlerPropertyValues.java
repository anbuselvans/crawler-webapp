package org.ghc.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class CrawlerPropertyValues {
    private static final Logger LOGGER = LoggerFactory.getLogger(CrawlerPropertyValues.class);

    public Properties getPropValues(String propFileName) throws IOException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            Properties properties = new Properties();

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            return properties;
        } catch (Exception exception) {
            LOGGER.error("Configuration file is not loaded '" + propFileName + "' properly", exception);
            throw new RuntimeException("Configuration file is not loaded '" + propFileName + "' properly");
        }

    }
}
