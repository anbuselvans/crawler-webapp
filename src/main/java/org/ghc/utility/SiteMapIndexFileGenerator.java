package org.ghc.utility;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.util.Date;

import com.redfin.sitemapgenerator.SitemapIndexGenerator;
import com.redfin.sitemapgenerator.SitemapIndexUrl;
import org.ghc.StartCrawlerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SiteMapIndexFileGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteMapIndexFileGenerator.class);

    public void generateSitemapIndexFile() {
        try {
            File outFile = new File(StartCrawlerController.props.getProperty("SiteMapOutPutDirectory") + "/sitemap_index.xml");
            SitemapIndexGenerator sig = new SitemapIndexGenerator("https://www.ghc.org", outFile);

            File[] siteMapHomeDir = new File(StartCrawlerController.props.getProperty("SiteMapOutPutDirectory")).listFiles(new FileFilter() {

                @Override
                public boolean accept(File file) {
                    return
                            file.isDirectory();
                }
            });
            if (siteMapHomeDir != null) {

                for (File file : siteMapHomeDir) {
                    try {
                        addSubDirInSiteMapIndexFile(file, sig);
                    } catch (Exception exception) {
                        LOGGER.error("Exception occurred while adding sub folder into siteMap index file", exception);
                    }
                }
            }
            sig.write();
        } catch (Exception e) {
            LOGGER.error("Exception occurred while generating siteMap index file", e);

        }
    }

    private void addSubDirInSiteMapIndexFile(File subDir, SitemapIndexGenerator sig) throws MalformedURLException {

        File subDirs[] = subDir.listFiles();
        if (subDirs != null) {
            for (File file : subDirs) {
                if (file.isFile()) {
                    try {
                        SitemapIndexUrl sitemapIndexUrl = new SitemapIndexUrl("https://www.ghc.org/" + subDir.getName() + "/" + file.getName(),
                                new Date(file.lastModified()));
                        sig.addUrl(sitemapIndexUrl);
                    } catch (Exception e) {
                        LOGGER.error("Exception occured while processing each subfolder sitemapt file", e);
                    }

                }
            }
        }

    }
}
