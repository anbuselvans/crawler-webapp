package org.ghc.crawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicCrawlerThread extends WebCrawler {

    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg"
            + "|png|mp3|mp3|zip|gz|pdf))$");

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicCrawlerThread.class);

    private BufferedWriter bufferedWriter;

    //String outputFile = "crawler_" + getMyId() + ".txt";

    @Override
    public void onStart() {
        try {
            if (System.getProperty("crawlOutputFolder") != null) {
                File crawlList = new File(System.getProperty("crawlOutputFolder"));
                setOutputFile(System.getProperty("crawlOutputFolder") + System.getProperty("file.separator") + "crawler_" + getMyId() + ".txt");
            } else {
                LOGGER.error(getMyId() + ": BasicCrawlerThread could not determine CrawlOutputFolder from System");
                return;
            }
        } catch (Exception e) {
            LOGGER.error(getMyId() + ": BasicCrawlerThread could not determine CrawlOutputFolder from System:" + e.getMessage());
            return;
        }
    }

    @Override
    public void onBeforeExit() {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                LOGGER.error(getMyId() + ": " + e.getMessage());
            }
        }
    }

    /**
     * This method receives two parameters. The first parameter is the page
     * in which we have discovered this new url and the second parameter is
     * the new url. You should implement this function to specify whether
     * the given url should be crawled or not (based on your crawling logic).
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return (!FILTERS.matcher(href).matches());
    }

    /**
     * This function is called when a page is fetched and ready
     * to be processed.
     */
    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        LOGGER.info(getMyId() + ": URL: " + url);

        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String text = htmlParseData.getText();
            String html = htmlParseData.getHtml();
            Set<WebURL> links = htmlParseData.getOutgoingUrls();
            Set<WebURL> cleanLinks = new HashSet<>();
            boolean uriIndexFound = false;

            for (WebURL link : links) {
                String uri = link.getURL();
                if (uri.contains(";jsessionid")) {
                    uri = uri.substring(0, uri.indexOf(";jsessionid"));
                    WebURL webURI = new WebURL();
                    webURI.setURL(uri);
                    cleanLinks.add(webURI);
                } else {
                    cleanLinks.add(link);
                }

                /**
                 *  check to see if the link ends with "/index.html", and if so,
                 *  change it to "/"
                 */
                if (uri.endsWith("/index.html")) {
                    LOGGER.info(getMyId() + ": found an index page link for " + uri);
                    uri = uri.substring(0, uri.length() - 10);
                    LOGGER.info(getMyId() + ": changed it to " + uri);
                }

                /**
                 *  check to see if there are web server shenanigans treating links that
                 *  do not end in a "/" as if they were /index.html, if so change them to end with a "/"
                 */
                if (!uri.endsWith("/")) {
                    String testUri = uri;
                    if (testUri.startsWith("http") && testUri.contains("://") && testUri.length() > 10) {
                        testUri = testUri.substring(testUri.indexOf("://") + 3);
                    }
                    if (testUri.contains("/")) {
                        testUri = testUri.substring(testUri.indexOf("/"));
                    }
                    if (!testUri.contains(".")) {
                        uriIndexFound = checkForIndex(uri);
                    }
                }

                Date modDate = new Date();
                long d = 0;
                String modDateValue = "unknown";
                try {
                    URL thisUrl = new URL(uri);
                    HttpURLConnection httpCon = (HttpURLConnection) thisUrl.openConnection();

                    d = httpCon.getLastModified();
                    if (d > 0) {
                        modDate = new Date(d);
                        modDateValue = modDate.toString();
                    }

                } catch (Exception e) {
                    LOGGER.info(getMyId() + ": could not determine modified date for " + uri);
                }

                String parentUri = link.getParentUrl();
                if (parentUri.contains(";jsessionid")) {
                    parentUri = parentUri.substring(0, parentUri.indexOf(";jsessionid"));
                }
                if (bufferedWriter != null) {
                    try {
                        bufferedWriter.write(uri + ", "
                                + modDateValue + ", "
                                + link.getDomain() + ", "
                                + parentUri + ", "
                                + link.getDepth() + ", "
                                + uriIndexFound
                                + System.lineSeparator());
                    } catch (IOException e) {
                        LOGGER.error(getMyId() + ": " + e.getMessage());
                    }
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.flush();
                } catch (IOException e) {
                    LOGGER.error(getMyId() + ": " + e.getMessage());
                }
            }
            htmlParseData.setOutgoingUrls(cleanLinks);

            LOGGER.info(getMyId() + ": Text length: " + text.length() + "  Html length: " + html.length() + " Number of " +
                    "outgoing links: " + cleanLinks.size());
        }
    }

    public void setOutputFile(String fname) {
        File crawlList = new File(fname);
        FileWriter fw;
        try {
            fw = new FileWriter(crawlList.getAbsoluteFile());
            bufferedWriter = new BufferedWriter(fw);
        } catch (IOException e) {
            LOGGER.error(getMyId() + ": " + e.getMessage());
        }

    }

    public boolean checkForIndex(String uri) {
        try {
            URL thisUrl = new URL(uri + "/");
            HttpURLConnection httpCon = (HttpURLConnection) thisUrl.openConnection();
            int d = httpCon.getResponseCode();
            if (d == 200) {
                return true;
            }
        } catch (Exception e) {
            LOGGER.info(getMyId() + ": link does not end with '/', and cannot find a default page by adding a '/' to the end of " + uri);
        }
        return false;
    }
}