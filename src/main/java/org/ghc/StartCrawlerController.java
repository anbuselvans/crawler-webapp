package org.ghc;

import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import edu.uci.ics.crawler4j.crawler.authentication.AuthInfo;
import edu.uci.ics.crawler4j.crawler.authentication.BasicAuthInfo;
import edu.uci.ics.crawler4j.crawler.authentication.FormAuthInfo;

import org.ghc.crawler.BasicCrawlerThread;
import org.ghc.utility.CrawlerPropertyValues;
import org.ghc.utility.RemoveDuplicatesUrl;
import org.ghc.utility.SiteMapIndexFileGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

import java.io.File;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import com.redfin.sitemapgenerator.WebSitemapGenerator;

import java.util.regex.*;

public class StartCrawlerController {

    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg"
            + "|png|mp3|mp3|zip|gz|pdf))$");

    private static final Logger LOGGER = LoggerFactory.getLogger(StartCrawlerController.class);

    public static Properties props = null;

    public static void main(String[] args) throws Exception {


       /* if (args.length != 1) {
            LOGGER.error("Required parameters are: ");
            LOGGER.error("\t name of properties file for runtime configuration");
            return;
        }*/
        RemoveDuplicatesUrl removeDuplicatesUrl = new RemoveDuplicatesUrl();
        SiteMapIndexFileGenerator siteMapIndexFileGenerator = new SiteMapIndexFileGenerator();
        CrawlerPropertyValues propertiesValues = new CrawlerPropertyValues();

        try {

            props = propertiesValues.getPropValues("crawler-www1.ghc.org.properties");
            //LOGGER.info("loaded properties from " + args[0]);
        } catch (Exception e) {
            LOGGER.error("first argument should be name of properties file for runtime configuration");
            return;
        }

        String crawlOutputFolder = "." + System.getProperty("file.separator") + "crawl";
        try {
            if (props.getProperty("CrawlOutputFolder") != null && !props.getProperty("CrawlOutputFolder").isEmpty()) {
                crawlOutputFolder = props.getProperty("CrawlOutputFolder");
                System.setProperty("crawlOutputFolder", crawlOutputFolder);
            } else {
                LOGGER.error("Properties file missing entry for CrawlOutputFolder");
                return;
            }
            LOGGER.info("CrawlOutputFolder: " + crawlOutputFolder);
        } catch (Exception e) {
            LOGGER.error("Properties file missing entry for CrawlOutputFolder");
            return;
        }

        boolean doCrawl = false;
        boolean createSiteMap = false;


        try {
            if (props.getProperty("DoCrawl") != null && !props.getProperty("DoCrawl").isEmpty()) {
                doCrawl = Boolean.parseBoolean(props.getProperty("DoCrawl"));
            }
            LOGGER.info("DoCrawl: " + doCrawl);
        } catch (Exception e) {
            LOGGER.error("Properties file bad entry for DoCrawl");
            return;
        }

        try {
            if (props.getProperty("CreateSiteMap") != null && !props.getProperty("CreateSiteMap").isEmpty()) {
                createSiteMap = Boolean.parseBoolean(props.getProperty("CreateSiteMap"));
            }
            LOGGER.info("CreateSiteMap: " + createSiteMap);
        } catch (Exception e) {
            LOGGER.error("Properties file bad entry for CreateSiteMap");
            return;
        }

        if (doCrawl) {
            /*
             * numberOfCrawlers shows the number of concurrent threads that should
             * be initiated for crawling.
             */
            int numberOfCrawlers = -1;
            String[] SEEDS = {"none"};
            int maxDepthOfCrawling = -1;
             /*
             * crawlStorageFolder is a folder where intermediate crawl data is
             * stored.
             */
            String crawlStorageFolder = "." + System.getProperty("file.separator") + "temp";
            int politenessDelay = 1000;
            int maxPagesToFetch = -1;


            try {
                if (props.getProperty("NumberOfCrawlers") != null && !props.getProperty("NumberOfCrawlers").isEmpty()) {
                    numberOfCrawlers = Integer.parseInt(props.getProperty("NumberOfCrawlers"));
                }
                LOGGER.info("NumberOfCrawlers: " + numberOfCrawlers);
            } catch (Exception e) {
                LOGGER.error("Properties file bad entry for NumberOfCrawlers");
                return;
            }

            try {
                if (props.getProperty("SEEDS") != null && !props.getProperty("SEEDS").isEmpty()) {
                    LOGGER.info("SEEDS: " + props.getProperty("SEEDS"));

                    if (props.getProperty("SEEDS").indexOf('|') > 0) {
                        SEEDS = props.getProperty("SEEDS").split("\\|");

                    } else {

                        SEEDS[0] = props.getProperty("SEEDS");
                    }
                }

            } catch (Exception e) {
                LOGGER.error("Properties file missing entry for SEEDS (delimited with |)");
                return;
            }

            try {
                if (props.getProperty("MaxDepthOfCrawling") != null && !props.getProperty("MaxDepthOfCrawling").isEmpty()) {
                    maxDepthOfCrawling = Integer.parseInt(props.getProperty("MaxDepthOfCrawling"));
                }
                LOGGER.info("MaxDepthOfCrawling: " + maxDepthOfCrawling);
            } catch (Exception e) {
                LOGGER.error("Properties file bad entry for MaxDepthOfCrawling");
                return;
            }

            try {
                if (props.getProperty("CrawlStorageFolder") != null && !props.getProperty("CrawlStorageFolder").isEmpty()) {
                    crawlStorageFolder = props.getProperty("CrawlStorageFolder");
                }
                LOGGER.info("CrawlStorageFolder: " + crawlStorageFolder);
            } catch (Exception e) {
                LOGGER.error("Properties file missing entry for CrawlStorageFolder");
                return;
            }


            try {
                if (props.getProperty("PolitenessDelay") != null && !props.getProperty("PolitenessDelay").isEmpty()) {
                    politenessDelay = Integer.parseInt(props.getProperty("PolitenessDelay"));
                }
                LOGGER.info("PolitenessDelay: " + politenessDelay);
            } catch (Exception e) {
                LOGGER.error("Properties file bad entry for PolitenessDelay ");
                return;
            }

            try {
                if (props.getProperty("MaxPagesToFetch") != null && !props.getProperty("MaxPagesToFetch").isEmpty()) {
                    maxPagesToFetch = Integer.parseInt(props.getProperty("MaxPagesToFetch"));
                }
                LOGGER.info("MaxPagesToFetch: " + maxPagesToFetch);
            } catch (Exception e) {
                LOGGER.error("Properties file bad entry for MaxPagesToFetch");
                return;
            }

            try {
                new File(crawlOutputFolder).mkdir();
            } catch (Exception e) {
                LOGGER.error("Properties file - could not create directory - bad entry for CrawlOutputFolder?");
                return;
            }

            try {
                new File(crawlStorageFolder).mkdir();
            } catch (Exception e) {
                LOGGER.error("Properties file - could not create directory - bad entry for CrawlStorageFolder?");
                return;
            }


            CrawlConfig config = new CrawlConfig();

            config.setCrawlStorageFolder(crawlStorageFolder);

            config.setPolitenessDelay(politenessDelay);

            config.setMaxDepthOfCrawling(maxDepthOfCrawling);

            // Unlimited number of pages can be crawled if -1
            config.setMaxPagesToFetch(maxPagesToFetch);

            // Data for the authentication methods
            String LOGIN_HOST = "none";
            String authUserName = "none";
            String authPassword = "none";
            String authNameUsername = "none";
            String authNamePassword = "none";

            if (props.getProperty("LOGIN_HOST") != null && !props.getProperty("LOGIN_HOST").isEmpty()) {
                LOGIN_HOST = props.getProperty("LOGIN_HOST");
                LOGGER.info("Using Authentication to: " + props.getProperty("LOGIN_HOST"));

                try {
                    if (props.getProperty("AuthUserName") != null && !props.getProperty("AuthUserName").isEmpty()) {
                        authUserName = props.getProperty("AuthUserName");
                    }
                    LOGGER.info("AuthUserName: " + authUserName);
                } catch (Exception e) {
                    LOGGER.error("Properties file missing entry for AuthUserName and LOGIN_HOST specified");
                    return;
                }


                try {
                    if (props.getProperty("AuthPassword") != null && !props.getProperty("AuthPassword").isEmpty()) {
                        authPassword = props.getProperty("AuthPassword");
                    }
                    LOGGER.info("AuthPassword: found");
                } catch (Exception e) {
                    LOGGER.error("Properties file missing entry for AuthPassword and LOGIN_HOST specified");
                    return;
                }


                try {
                    if (props.getProperty("AuthNameUsername") != null && !props.getProperty("AuthNameUsername").isEmpty()) {
                        authNameUsername = props.getProperty("AuthNameUsername");
                    }
                    LOGGER.info("AuthNameUsername: " + authNameUsername);
                } catch (Exception e) {
                    LOGGER.error("Properties file missing entry for AuthNameUsername (name of auth name field) and " +
                            "LOGIN_HOST " +
                            "specified");
                    return;
                }

                try {
                    if (props.getProperty("AuthNamePassword") != null && !props.getProperty("AuthNamePassword").isEmpty()) {
                        authNamePassword = props.getProperty("AuthNamePassword");
                    }
                    LOGGER.info("AuthNamePassword: " + authNamePassword);
                } catch (Exception e) {
                    LOGGER.error("Properties file missing entry for AuthNamePassword (name of auth password field) and " +
                            "LOGIN_HOST specified");
                    return;
                }


                AuthInfo authInfo1 = new FormAuthInfo(authUserName, authPassword, LOGIN_HOST,
                        authNameUsername, authNamePassword);
                config.addAuthInfo(authInfo1);
                AuthInfo authInfo2 = new BasicAuthInfo(authUserName, authPassword, LOGIN_HOST);
                config.addAuthInfo(authInfo2);

            }


            /**
             * Update the logic inorder to generate the sitemap for each seed then proceed to the next seed
             * Move the logic to functions inorder to simplify modularity
             */

            /*
             * For each crawl, you need to add some seed urls. These are the first
             * URLs that are fetched and then the BasicCrawlerThread starts following links
             * which are found in these pages
             */
            if (SEEDS.length > 0) {


                for (String seed : SEEDS) {
                    try {

                        String seedUrls[] = seed.split("/");
                        String seedFile = seedUrls[seedUrls.length - 1].toString();
                        File fileOne = new File(".\\src\\main\\resources\\" + seedFile + ".properties");

                        if (fileOne.exists()) {
                            props = propertiesValues.getPropValues(seedFile + ".properties");
                            config.setMaxDepthOfCrawling(Integer.parseInt(props.getProperty("MaxDepthOfCrawling")));


                        } else {
                            props = propertiesValues.getPropValues("crawler-www1.ghc.org.properties");
                            config.setMaxDepthOfCrawling(Integer.parseInt(props.getProperty("MaxDepthOfCrawling")));

                        }

                        PageFetcher pageFetcher = new PageFetcher(config);
                        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
                        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
                        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

                        //Execute the sitemap generation logic for each seed
                        LOGGER.info("SEED=" + seed);
                        controller.addSeed(seed);

                    /*
             * Start the crawl.
             */


                        //controller.startNonBlocking(BasicCrawlerThread.class, numberOfCrawlers);
                        controller.start(BasicCrawlerThread.class, numberOfCrawlers);

                        // Wait for 30 seconds
                        Thread.sleep(30 * 1000);

                        // Send the shutdown request and then wait for finishing
                        controller.shutdown();
                        controller.waitUntilFinish();
                        //update process

                        //consolidate all crawler files into one file
                        // concatenateCrawls(crawlOutputFolder);

                        //remove duplicate URIs
                        removeDuplicatesUrl.removeDuplicateUrls();


                        LOGGER.info("CRAWLING COMPLETED");

                        /**
                         * logic for sitemap generation for indivdual seed starts here
                         */
                        if (createSiteMap) {
                            LOGGER.info("CREATING SITE MAP");
                            String siteMapBaseURI = "none";
                            File siteMapOutPutDirectory = null;
                            File siteMapCrawlInputFile = null;

                            try {
                                if (props.getProperty("SiteMapBaseURI") != null && !props.getProperty("SiteMapBaseURI").isEmpty()) {
                                    siteMapBaseURI = props.getProperty("SiteMapBaseURI");
                                }
                                LOGGER.info("SiteMapBaseURI: " + siteMapBaseURI);
                            } catch (Exception e) {
                                LOGGER.error("Properties file missing entry for SiteMapBaseURI (base URI of website) and " +
                                        "CreateSiteMap specified");
                                return;
                            }
                            try {
                                if (props.getProperty("SiteMapOutPutDirectory") != null && !props.getProperty("SiteMapOutPutDirectory")
                                        .isEmpty()) {
                                    //get the use from the seed url and then make a directory
                                    String seedFolderName[] = seed.split("/");
                                    siteMapOutPutDirectory = new File(props.getProperty("SiteMapOutPutDirectory") + seedFolderName[seedFolderName.length - 1].toString());

                                    siteMapOutPutDirectory.mkdirs();
                                    LOGGER.info("sitemap subfolder created successfully");

                                }
                                if (siteMapOutPutDirectory == null) {
                                    LOGGER.error("Properties file missing entry for SiteMapOutPutDirectory (folder to place generated " +
                                            "site map) and CreateSiteMap specified");
                                    return;
                                }
                                LOGGER.info("SiteMapOutPutDirectory: " + siteMapOutPutDirectory);
                            } catch (Exception e) {
                                LOGGER.error("Properties file bad entry for SiteMapOutPutDirectory (folder to place generated " +
                                        "site map) and CreateSiteMap specified");
                                return;
                            }

                            try {
                                if (props.getProperty("SiteMapCrawlInputFile") != null && !props.getProperty("SiteMapCrawlInputFile")
                                        .isEmpty()) {
                                    siteMapCrawlInputFile = new File(props.getProperty("SiteMapCrawlInputFile"));
                                }
                                if (siteMapOutPutDirectory == null) {
                                    LOGGER.error("Properties file missing entry for SiteMapCrawlInputFile (file containing URIs of " +
                                            "website, ) and " +
                                            "CreateSiteMap specified");
                                    return;
                                }
                                LOGGER.info("SiteMapCrawlInputFile: " + siteMapCrawlInputFile);
                            } catch (Exception e) {
                                LOGGER.error("Properties file bad entry for SiteMapCrawlInputFile (file containing URIs of " +
                                        "website, ) and " +
                                        "CreateSiteMap specified");

                            }

                            Path path = Paths.get(siteMapCrawlInputFile.getPath());
                            List<String> lines = null;
                            try {
                                LOGGER.info("reading SiteMapCrawlInputFile " + path.getFileName().toString());
                                lines = Files.readAllLines(path, StandardCharsets.UTF_8);
                            } catch (Exception e) {
                                LOGGER.error("CANNOT READ SiteMapCrawlInputFile " + siteMapCrawlInputFile);

                            }
                            if (lines == null || lines.size() == 0) {
                                LOGGER.error("SiteMapCrawlInputFile empty or null: " + siteMapCrawlInputFile);
                                // return;
                            }


                            WebSitemapGenerator wsg = WebSitemapGenerator.builder(siteMapBaseURI, siteMapOutPutDirectory)
                                    .gzip(true).build(); // enable gzipped output

                            //List<Object> L = controller.getCrawlersLocalData();

                            //load all rules into a usable structure

                            Map<Pattern, Map<String, String>> rules = new HashMap<Pattern, Map<String, String>>();
                            Set<Pattern> excludeRules = new HashSet<>();

                            int max = 500;
                            for (int i = 0; i < max; i++) {
                                try {
                                    if (props.getProperty("rule" + i) != null && !props.getProperty("rule" + i)
                                            .isEmpty()) {

                                        String[] opts = props.getProperty("rule" + i).split("[|]");

                                        if (opts.length < 2 || opts.length > 4 || opts.length == 3) {
                                            LOGGER.error("rule " + i + " invalid number of options");
                                            return;
                                        } else {
                                            Pattern r = Pattern.compile(opts[0]);
                                            String[] vals = Arrays.copyOfRange(opts, 1, opts.length);
                                            if (!"exclude".equals(vals[0].toString())) {
                                                Map<String, String> attrs = new HashMap<String, String>(3);
                                                attrs.put("lastmod", opts[1]);
                                                attrs.put("priority", opts[2]);
                                                attrs.put("changefreq", opts[3]);
                                                rules.put(r, attrs);
                                                LOGGER.info("added rule " + i);
                                            } else {
                                                excludeRules.add(r);
                                                LOGGER.info("found exclude rule " + i);

                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    LOGGER.error("ERROR: loading rule: " + i + " " + e.getMessage());
                                    return;

                                }
                            }

                            //DateTimeFormat dateFormat = DateTimeFormatter.ofPattern ("yyyy-MM-dd");

                            try {
                                Set<WebSitemapUrl> finalUrls = new LinkedHashSet<WebSitemapUrl>();
                                Set<String> seenUrls = new LinkedHashSet<String>();

                                for (Object ent : lines) {

                                    String[] record = ent.toString().split(",");
                                    WebSitemapUrl url = null;

                                    if (record != null && record.length > 0 && record[0] != null && !FILTERS.matcher(record[0]).matches()) {
                                        record[0] = record[0].trim();

                                        if (record[0].startsWith(siteMapBaseURI) && !FILTERS.matcher(record[0]).matches()) {
                                            //see if it should be excluded
                                            boolean exclude = false;
                                            for (Pattern pat : excludeRules) {
                                                try {
                                                    Matcher m = pat.matcher(record[0]);
                                                    if (m.find()) {
                                                        exclude = true;
                                                    }
                                                } catch (final Throwable ignore) {
                                                    LOGGER.error("ERROR working on SiteMap: " + ignore.getMessage());
                                                }
                                            }
                                            if (!exclude) {
                                                //find first matching rule
                                                for (Pattern pat : rules.keySet()) {
                                                    try {
                                                        Matcher m = pat.matcher(record[0]);

                                                        if (m.find()) {
                                                            Date lastmod = new Date();
                                                            double priority = 1.0;
                                                            ChangeFreq changefreq = ChangeFreq.WEEKLY;

                                                            Map<String, String> mAttrs = rules.get(pat);
                                                            try {
                                                                if ("file".equals(mAttrs.get("lastmod").trim())) {
                                                                    //use the timestamp on the file if available
                                                                    if ("unknown".equals(record[1].trim())) {
                                                                        lastmod = null;
                                                                    } else {

                                                                        lastmod = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(record[1].trim().toString())));

                                                                    }
                                                                } else if ("now".equals(mAttrs.get("lastmod").trim())) {
                                                                    lastmod = new Date();
                                                                } else if ("unknown".equals(mAttrs.get("lastmod").trim())) {
                                                                    lastmod = null;
                                                                } else {
                                                                    lastmod = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(mAttrs.get("lastmod")).toString()));

                                                                }
                                                            } catch (Exception e) {
                                                                LOGGER.error("CANNOT parse lastmod for SiteMap! " + e.getMessage());
                                                            }

                                                            try {
                                                                priority = Double.parseDouble(mAttrs.get("priority"));
                                                            } catch (Exception e) {
                                                                LOGGER.error("CANNOT parse priority for SiteMap! " + e.getMessage());
                                                            }

                                                            try {
                                                                String cf = mAttrs.get("changefreq").toLowerCase();
                                                                if ("always".equals(cf)) {
                                                                    changefreq = ChangeFreq.ALWAYS;
                                                                } else if ("hourly".equals(cf)) {
                                                                    changefreq = ChangeFreq.HOURLY;
                                                                } else if ("daily".equals(cf)) {
                                                                    changefreq = ChangeFreq.DAILY;
                                                                } else if ("weekly".equals(cf)) {
                                                                    changefreq = ChangeFreq.WEEKLY;
                                                                } else if ("monthly".equals(cf)) {
                                                                    changefreq = ChangeFreq.MONTHLY;
                                                                } else if ("yearly".equals(cf)) {
                                                                    changefreq = ChangeFreq.YEARLY;
                                                                } else if ("never".equals(cf)) {
                                                                    changefreq = ChangeFreq.NEVER;

                                                                }
                                                            } catch (Exception e) {
                                                                LOGGER.error("CANNOT parse changefreq for SiteMap! " + e.getMessage());
                                                            }

                                                            if (lastmod == null) {

                                                                url = new WebSitemapUrl.Options(record[0])
                                                                        .priority(priority).changeFreq(changefreq).build();
                                                            } else {

                                                                url = new WebSitemapUrl.Options(record[0])
                                                                        .lastMod(new Date()).priority(priority).changeFreq(changefreq).build();
                                                            }
                                                            if (!seenUrls.contains(record[0])) {
                                                                finalUrls.add(url);
                                                                seenUrls.add(record[0]);
                                                            }
                                                        }
                                                    } catch (final Throwable ignore) {
                                                        LOGGER.error("ERROR working on SiteMap: " + ignore.getMessage());
                                                    }
                                                }
                                            }
                                            if (url == null && !exclude) { //we did not find a match for a rule or an exclusion
                                                if (!seenUrls.contains(record[0])) {
                                                    url = new WebSitemapUrl.Options(record[0])
                                                            .lastMod(new Date()).priority(0.8).changeFreq(ChangeFreq.WEEKLY).build();
                                                    // this will configure the URL with lastmod=now, priority=1.0, changefreq=hourly
                                                    finalUrls.add(url);
                                                    seenUrls.add(record[0]);
                                                }
                                            }

                                        }
                                    }
                                }

                                for (WebSitemapUrl u : finalUrls) {

                                    wsg.addUrl(u);
                                }
                                wsg.write();

                            } catch (Exception e) {
                                LOGGER.error("CANNOT write SiteMap! " + e.getMessage());

                            }
                            LOGGER.info("SITE MAP COMPLETE");
                        }

                        /**
                         * logic for sitemap for individual item ends here
                         */
                    } catch (Exception exception) {
                        LOGGER.error("Exception occurred while generating sitemap for given seed=" + seed, exception);

                    }

                }
                siteMapIndexFileGenerator.generateSitemapIndexFile();

            } else {
                LOGGER.error("Missing Seeds");
                return;
            }


        }


    }


}